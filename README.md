# Readme

This export creates a user-friendly (excel) version history of the [Jobtech Taxonomy](https://jobtechdev.se/sv/komponenter/jobtech-taxonomy) for humans to read.

It uses the [changelog endpoint](http://taxonomy.api.jobtechdev.se/v1/taxonomy/graphiql) in the Taxonomy Graph-QL API and applies some quality of life improvements for the reader.

See [version_files folder](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-version-history/-/tree/main/version_files) for version and type specific excel files where e.g. `version 19` implies the "from" version is set to 18.

The repository will be updated with files for `skill` and `occupation-name` concepts each time a new version is published.
So if you are only interested in those two concept-types there is no need to run this program yourself.

## About Taxonomy Version History

The Taxonomy Version History provides more accessible documentation of the work we do with the contents of Jobtech Taxonomy.
Some Taxonomy users may want to get information on the latest updates concerning a specific concept type, e.g. occupation-name or skill.
The current changelog API is good and usable for machines but not so much for people. 
The Taxonomy Version History creates an excel detailing changes (new concepts, updated concepts, relations etc.) within a concept type between two versions,
with the hopes of making events tied to those parameters more transparent.

### What it is not

The Taxonomy Version History should not be seen as a complete delta or exhaustive description of changes between two versions.

Even though most changes can be found here, they may be merged or interpreted in a way that makes them less useful for programmatic purposes.

## Getting started

If for some reason you want to use the export  yourself, follow these steps:

First, clone this repository:

`git clone git@gitlab.com:arbetsformedlingen/taxonomy-content/taxonomy-version-history.git`

To export a version history log, where versions and types may be modified to values existing in the taxonomy (see further down), use:

`clj -X:export-version-history :from-version 17 :to-version 18 :type occupation-name`

By not including any arguments the settings will default to the configurations found in settings/config.edn

`clj -X:export-version-history`

Only consecutive versions should be provided, it is likely the request to the API will be too demanding otherwise.

### Prerequisites

- Clojure CLI (reasonably late version)
- Java

## Settings

### Config

The config.edn specifies the contents of the GraphQL-query. 
By default no version parameters are provided (meaning the export will look for the latest versions) and `:type` is set to occupation-name. The parameter `:include-attribs` specifies which attributes to include in each concept.

The  `:limit` parameter represents the number of items to be returned by each request. 
Modify this value if the export takes too long to complete (try higher) or if the API returns a timeout (try lower) message.  

### Map-order

Used for ordering the each map's (or log event's) key-value pair.
Maybe redundant but it makes the exported excel more readable.

## Taxonomy types and versions

Taxonomy types that may be included in the script can be found here: http://taxonomy.api.jobtechdev.se/v1/taxonomy/main/concept/types

Taxonomy versions that may be included in the script can be found here: http://taxonomy.api.jobtechdev.se/v1/taxonomy/main/versions

## Known issues

The changelog response gets very lengthy between versions of unusually high activity.
These requests will take a while to process. Modifying the `:limit` config parameter might resolve some situations.

## License

See [LICENSE](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-version-history/-/blob/main/LICENSE)

## Contact

david.norman@arbetsformedlingen.se