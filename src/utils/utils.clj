(ns utils.utils
  (:require [clojure.string :as str]
            [clojure.edn :as edn]
            [dk.ative.docjure.spreadsheet :as dox]
            [babashka.fs :as fs]))

(defn read-edn
  [file]
  (edn/read-string (slurp file)))

(defn time->date
  [time-str]
  (re-find #"\d{4}-\d{2}-\d{2}" time-str))

(defn split-types
  "Split types for query insertion."
  [types]
  (apply str (interpose " " (map name types))))

(defn capitalize-term
  [s]
  (str/capitalize s))

(defn maps->excel
  [coll path]
  (let [columns (keys (first coll))
        rows (map vals coll)
        table (cons columns rows)
        wb (dox/create-workbook "Log" table)]
    (dox/save-workbook! path wb)))

(defn snake-it
  [s]
  (str/replace s "-" "_"))

(defn get-version-path
  [to-version]
  (str "version_files/version_" to-version))

(defn get-type-path
  [to-version type]
  (str "version_files/version_" to-version "/" (snake-it type)))

(defn get-final-path
  [{:keys [to-version type]}]
  (str "version_files/version_" to-version "/" (snake-it type) "/" (snake-it type) "_" to-version ".xlsx"))

(defn dir?
  ([to-version]
   (let [path (get-version-path to-version)]
     (fs/directory? path)))
  ([to-version type]
   (let [path (get-type-path to-version type)]
     (fs/directory? path))))

(defn create-dir
  "Provide with version and/or type to create new directory
   in which to store version logs. File structure looks like:
     |- version_files
     |--- version
     |----- type
   If not found, version directory will need to be created first."
  ([to-version]
   (let [path (get-version-path to-version)]
     (fs/create-dir path)))
  ([to-version type]
   (let [path (get-type-path to-version type)]
     (fs/create-dir path))))

(defn handle-dirs
  [{:keys [to-version type]}]
  (cond (not (dir? to-version))
        (do (create-dir to-version)
            (create-dir to-version type))
        (not (dir? to-version type))
        (create-dir to-version type)))

