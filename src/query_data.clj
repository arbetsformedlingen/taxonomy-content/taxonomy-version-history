(ns query-data
    (:require [clj-http.client :as client]
              [utils.utils :as u]
              [configs :refer :all]
              [graphql :refer :all]))

(defn query-graph-ql
  "Takes queried data and does a bit of un-nesting
   based on keys specified in the layers vector."
  [query endpoint layers & api-key]
  (-> (client/get (str endpoint query)
                  {:headers (when api-key {:api-key api-key})
                   :as      :json})
      (get-in layers)))

(def version-query
  (-> paths
      :request-urls
      u/read-edn
      :versions))

(def latest-version
  (let [q (client/get version-query {:as :json})]
    (-> q
        :body
        last
        :taxonomy/version)))

(defn get-concept
  "id is string, attribs a vector of strings."
  [id attribs]
  (let [attribs-split attribs]
    (query-graph-ql (query-on-id id attribs-split)
                    graphql-endpoint
                    [:body :data :concepts])))

(defn complete-config
  "If any values for from-version or to-version are missing,
  the latest and next latest versions will be used."
  [{:keys [from-version to-version] :as all}]
  (println (str "Defaulting to config: " all))
  (if (some #(nil? %) [from-version to-version])
    (assoc all :from-version (- latest-version 1)
               :to-version latest-version)
    all))

(defn loop-query
  "Splits request in order to not cause a server timeout.
   See config.edn for settings."
  [type attribs from-v to-v limit]
  (loop [offset 0
         data []]
    (let [request
          (query-graph-ql
            (query-log type attribs from-v to-v limit offset)
            graphql-endpoint
            [:body :data :changelog])]
      (println "Fetching data chunk...")
      (if (empty? request)
        data
        (recur (+ limit offset)
               (concat data request))))))

(defn get-log
  "Queries changelog data based on config.

  A large amount of updates for the specified type between the
  specified versions means a longer response time."
  [{:keys [from-version to-version type include-attribs limit]}]
  (loop-query
    type
    (u/split-types include-attribs)
    from-version
    to-version
    limit))

