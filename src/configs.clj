(ns configs
  (:require [utils.utils :as u]))

(def paths
  (u/read-edn "settings/paths.edn"))

(def config
  (u/read-edn "settings/config.edn"))

(def map-order
  (-> paths
      :map-order
      u/read-edn))

(def types-and-translations
  (-> paths
      :types
      u/read-edn))

(def relation-types
  (-> types-and-translations
      :relation-types
      keys))

(def graphql-endpoint
  (-> paths
      :graphql-endpoint))

(def kw-attribs
  (:include-attribs config))

(def attributes
  (u/split-types kw-attribs))

(def predefined-translations
  ;TODO maybe move?
  "Puts all translations in a map for easy access."
  (apply merge (vals types-and-translations)))

(def custom-translations
  ;TODO maybe move?
  "These will be used to translate keys (attribs) of related concepts,
   in a log event, in order to distinguish them from the main concept,
   which is denoted by the ordinary attrib names."
  (zipmap kw-attribs
          (map #(str (predefined-translations %) ": relaterat begrepp")
               kw-attribs)))