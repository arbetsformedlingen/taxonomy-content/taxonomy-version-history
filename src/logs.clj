(ns logs
  (:require
    [clojure.set :as set]
    [utils.utils :as u]
    [configs :refer :all]
    [query-data :refer :all]))

(defn relation?
  [attrib]
  (some #(= % attrib) relation-types))

(defn handle-concept
  [{:keys [concept] :as all}]
  (-> all
      (merge concept)
      (dissoc :concept)))

(defn handle-updated-relation
  [changes]
  (let [new-val (:new_value changes)
        old-val (:old_value changes)]
    (if (< (count old-val) (count new-val))
      {:change_type "new_relation"
       :relation_to (-> (set/difference (set new-val) (set old-val))
                        first
                        :id)}
      {:change_type   "removed_relation"
       :relation_from (-> (set/difference (set old-val) (set new-val))
                          first
                          :id)})))

(defn handle-updated-attrib
  [changes attrib]
  {:change_type (str "updated_" attrib)
   :new_value   (str (:new_value changes))
   :old_value   (str (:old_value changes))})

(defn handle-updated-row
  [row]
  (let [changes (-> row :changes first)
        attrib (:attribute changes)]
    ; attrib can either be relation or concept-specific attribute
    (if (relation? attrib)
      (-> row
          (merge (handle-updated-relation changes))
          (handle-concept)
          (assoc :relation_type attrib)
          (dissoc :changes))
      (-> row
          (merge (handle-updated-attrib changes attrib))
          (handle-concept)
          (dissoc :changes)))))

(defn dispatch-row
  [row]
  (case (:event row)
    "Updated" (handle-updated-row row)
    "Created" (handle-concept row)
    "Deprecated" (handle-concept row)
    "Commented" (handle-concept row)))

(defn parse-row
  "A row represents a single log event in the changelog."
  [{:keys [concept] :as all}]
  (let [id (-> concept :id)
        row (-> all
                (select-keys [:concept :event :timestamp :comment :changes])
                dispatch-row
                vector)]
    {id row}))

(defn coll->map
  "Returns a map where concept-ids acts as keys and
  all events that can be tied to a concept as its values."
  [coll]
  (apply merge-with into
         (map parse-row coll)))

(defn get-translations
  "Uses the provided term if no translation can be found."
  [terms translations]
  (let [terms-translated (map #(get translations % %) terms)]
    (zipmap terms terms-translated)))

(defn translate-keys
  [m translations-table]
  (set/rename-keys
    m
    (get-translations (keys m) translations-table)))

(defn translate-vals
  "Renames values in a map based on provided translations table."
  [m translations-table]
  (let [translations (get-translations (vals m) translations-table)]
    (reduce-kv
      (fn [m k v]
        (assoc m k (translations v)))
      {} m)))

(defn conform-date
  [m]
  (assoc m :timestamp (u/time->date (m :timestamp))))

(defn add-related-concept
  [{:keys [relation_to] :as all}]
  (let [concept (get-concept relation_to attributes)]
    (-> concept
        first
        (translate-keys custom-translations)
        (merge all))))

(defn get-all-keys
  "Each key found throughout the collection needs to be present in each map."
  [coll]
  (set
    (flatten
      (map #(keys %)
           coll))))

(defn beautify-contents
  [m]
  (-> m
      conform-date
      add-related-concept
      (translate-keys predefined-translations)
      (translate-vals predefined-translations)))

(defn populate-keys
  "If key not present in map, add it with nil as val."
  [m ks]
  (let [these-ks (keys m)
        diff-ks (set/difference (set ks) (set these-ks))
        nil-ks (zipmap diff-ks (repeat (count diff-ks) nil))]
    (merge m nil-ks)))

(defn sort-map
  "Keys unknown to 'map-order' will be placed last in the map."
  [m]
  (let [keys-ordered (concat map-order (set/difference (set (keys m)) (set map-order)))
        keys->idx (zipmap keys-ordered (range))
        order-fn (fn [x y] (< (keys->idx x) (keys->idx y)))
        m-sorted (into (sorted-map-by order-fn) m)]
    m-sorted))

(defn sync-map
  ; I've switched the excel library since I wrote these,
  ; so it is possible some parts can be omitted
  "Some finishing touches giving excel export the Proper Look."
  [m coll]
  (let [all-keys (get-all-keys coll)]
    (-> m
        (populate-keys all-keys)
        sort-map)))

(defn sync-maps
  [coll]
  (reduce
    (fn [acc m]
      (conj acc (sync-map m coll)))
    [] coll))

(defn export-version-history
  "Creates an excel file with the modified logs for changes
  of concepts within a certain concept types between two versions.

  Pass :from-version, :to-version and :type when executing
  the function if other values than those found in settings/config.edn
  are needed.

  If other concept attributes are needed in the graphql query,
  settings/config.edn, will have to be updated.

  Defaults to config.edn if no args present."
  [& [opts]]
  (let [attribs (:include-attribs config)
        limit (:limit config)
        cfg (if opts (assoc opts :include-attribs attribs :limit limit)
                     (complete-config config))
        log (coll->map (get-log cfg))
        table (->> log
                   vals
                   flatten
                   (map beautify-contents)
                   sync-maps)]
    (do (u/handle-dirs cfg)
        (u/maps->excel
          table
          (u/get-final-path cfg))
        (println
          "Done exporting to" (u/get-final-path cfg)))))


