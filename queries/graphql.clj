(ns graphql)

(defn query-log
  [type attribs from-v to-v limit offset]
  (format
    "query log {
    changelog(type: \"%1$s\", from: %3$s, to: %4$s, limit: %5$s, offset: %6$s) {
      event: __typename
      timestamp
      user
      comment
      concept {
        %2$s
      }
      ... on Updated {
        changes {
          attribute
          new_value
          old_value
        }
      }
      ... on Created {
        concept {
          %2$s
        }
      }
    }
  }" type attribs from-v to-v limit offset))

(defn query-on-id
  [id attribs]
  (format
    "query MyQuery {
     concepts(id: \"%s\") {
     %s
  }
}
 " id attribs))


